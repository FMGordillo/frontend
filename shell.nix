let
  pkgs = import <nixpkgs> {};

  shell = pkgs.mkShell {
    nativeBuildInputs = with pkgs; [
      nodePackages.pnpm
      nodejs_20
    ];
  };

in shell

