export const languages = {
  en: "English",
  es: "Español",
};

export const defaultLang = "en";

export const ui: Record<keyof typeof languages, any> = {
  en: {
    language_label: "Language",
    subtitle_1: "Building the future together:",
    subtitle_2: "One block at a time",
    lets_talk: "Let's talk",
    about_projects: "About our projects",
    about_us: "About us",
    about_team: "The team",
    what_we_do: "What we do",
    our_industries: "We focus on",
    contact_us: "Contact us",
    "industry_sections.finance-description":
      "Our experience in technologies like JavaScript and TypeScript allows us to offer innovative solutions for the financial industry.",
    "industry_sections.finance-title": "Finance",
    "industry_sections.health-description":
      "We explore technologies like Web3 to enhance healthcare and patient health tracking in collaboration with healthcare professionals.",
    "industry_sections.health-title": "Health",
    "industry_sections.legal-description":
      "We work in collaboration with legal professionals to develop applications that simplify processes and optimize case management.",
    "industry_sections.legal-title": "Legals",
    our_projects: "Our projects",
    "projects.amaranto.title": "Amaranto",
    "projects.amaranto.description":
      "A patient and clinical records manager, with the assistance of AI to automate your tasks effectively",
    "projects.secret_diary.title": "Secret Diary",
    "projects.secret_diary.description":
      "Create an user and start register your deepest thoughts, but using only your ETH address. No personal information in our systems",
    "projects.finance_manager.title": "Finance Manager",
    "projects.finance_manager.description":
      "Demo project with user's transactions, using GPT to create custom queries in natural language",
    "sections.blockchain-title": "Innovation in Blockchain",
    "sections.blockchain-text":
      "We are experts in Blockchain application development, crafting innovative and secure solutions that drive the technological revolution.",
    "sections.cybersecurity-title": "Protecting your Assets",
    "sections.cybersecurity-text":
      "We specialize in strengthening your digital assets against evolving threats, implementing cutting-edge security protocols to keep your systems and data safe.",
    "sections.skills-title": "Shaping the Future",
    "sections.skills-text":
      "With diverse expertise in JavaScript, TypeScript, and more, we lead innovation in Web3, HTMX, and Svelte to take your projects to the next level.",
    "form-title": "Contact us",
    "form-subtitle":
      "Fill out the form below and we'll get back to you as soon as possible",
    "form-label-name": "Name",
    "form-placeholder-name": "Enter your name",
    "form-label-organization": "Organization",
    "form-placeholder-organization": "Enter your organization name",
    "form-placeholder-email": "Enter your email",
    "form-label-msg": "Leave us a message",
    "form-placeholder-msg": "Enter your message",
    "form-button-text": "Send",
    "snackbar-ok": "Message sent successfully!",
    "snackbar-failed": "Whops! Something went wrong",
    rights: "All rights reserved",
    "spain-street": "Spain: Moreto 34 St., Mataró, Barcelona",
    "arg-street": "Argentina: Carlos Pellegrini 1149 Buenos Aires",
    "about-us.mission-title": "Our mission",
    "about-us.timeline-title": "Our history",
    "about-us.timeline-1-title": "ChiroTech was created",
    "about-us.timeline-1-description":
      "We acknowledge the increasing demand for human collaboration in the digital age. In an environment where innovations in Artificial Intelligence are constantly evolving, this need becomes more critical than ever.",
    mission:
      "Developing excellent products while always keeping the human touch",
    "about-us.timeline-2-title": "FavForMe was created",
    "about-us.timeline-2-description":
      "We established a foundation aimed at supporting those who provide assistance by creating solidarity crowdfunding campaigns in collaboration with socially responsible companies.",
    "about-us.timeline-2-button": "See site",
    "about-us.timeline-3-title": "Graduated at Founder Institute Buenos Aires",
    "about-us.timeline-3-description":
      "We joined the three-month accelerator program, connecting with the vibrant entrepreneurial ecosystem of Buenos Aires and establishing connections with potential partners.",
    "about-us.footer-title": "We want to know your project, let's talk",
    "about-us.footer-meeting": "Schedule a meeting",
    "about-us.footer-mail": "Send us an email",
  },
  es: {
    language_label: "Lenguaje",
    subtitle_1: "Construyendo el futuro juntos:",
    subtitle_2: "Un bloque a la vez",
    lets_talk: "Charlemos",
    about_us: "Sobre nosotros",
    about_projects: "Sobre nuestros proyectos",
    about_team: "El equipo",
    our_industries: "Nuestro enfoque",
    what_we_do: "Qué hacemos",
    contact_us: "Contáctanos",
    "industry_sections.finance-description":
      "Nuestra experiencia en tecnologías como JavaScript y TypeScript nos permite ofrecer soluciones innovadoras para la industria financiera.",
    "industry_sections.finance-title": "Finanzas",
    "industry_sections.health-description":
      "Exploramos tecnologías como Web3 para mejorar la atención médica y el seguimiento de la salud de los pacientes en colaboración con profesionales de la salud.",
    "industry_sections.health-title": "Salud",
    "industry_sections.legal-description":
      "Trabajamos en colaboración con profesionales legales para desarrollar aplicaciones que simplifiquen procesos y optimicen la gestión de casos.",
    "industry_sections.legal-title": "Legales",
    our_projects: "Que hicimos",
    "projects.amaranto.title": "Amaranto",
    "projects.amaranto.description":
      "Gestor de registros de pacientes y clínicos, con la asistencia de la inteligencia artificial para automatizar tus tareas de manera efectiva",
    "projects.secret_diary.title": "Diario Secreto",
    "projects.secret_diary.description":
      "Crea un usuario y comienza a registrar tus pensamientos más profundos, pero utiliza solo tu dirección ETH. No se incluirá información personal en nuestros sistemas",
    "projects.finance_manager.title": "Gestor de Finanzas",
    "projects.finance_manager.description":
      "Proyecto de demostración con las transacciones del usuario, utilizando GPT para crear consultas personalizadas en lenguaje natural",
    "blockchain-title": "Innovación en Blockchain",
    "blockchain-text":
      "Somos expertos en desarrollo de aplicaciones Blockchain, creando soluciones innovadoras y seguras que impulsan la revolución tecnológica.",
    "cybersecurity-title": "Protegiendo tus activos",
    "cybersecurity-text":
      "Nos especializamos en fortalecer tus activos digitales contra amenazas cambiantes, implementando protocolos de seguridad de vanguardia para mantener tus sistemas y datos a salvo.",
    "skills-title": "Formando el Futuro",
    "skills-text":
      "Con experiencias variadas en JavaScript, TypeScript y más, lideramos la innovación en Web3, HTMX y Svelte para llevar tus proyectos al siguiente nivel.",
    "form-title": "Contáctanos",
    "form-subtitle": "Complete el formulario y nos comunicaremos cuanto antes!",
    "form-label-name": "Nombre",
    "form-placeholder-name": "Ingrese su nombre",
    "form-label-organization": "Organización",
    "form-placeholder-organization": "Ingrese el nombre de su empresa",
    "form-placeholder-email": "Ingrese su email",
    "form-label-msg": "Deje su mensaje",
    "form-placeholder-msg": "Ingrese su mensaje",
    "form-button-text": "Enviar",
    "snackbar-ok": "Mensaje enviado con éxito!",
    "snackbar-failed": "Error al enviar mensaje",
    rights: "Todos los derechos reservados",
    "spain-street": "España: Calle Moreto 34, Mataró, Barcelona",
    "arg-street": "Argentina: Carlos Pellegrini 1149, Buenos Aires",
    "timeline-title": "Nuestra historia",
    "mission-title": "Nuestra misión",
    mission:
      "Desarrollar productos excelentes manteniendo siempre la esencia humana",
    "timeline-1-title": "Fundamos ChiroTech",
    "timeline-1-description":
      "Reconocemos la creciente demanda de colaboración humana en la era digital. En un entorno donde las innovaciones en Inteligencia Artificial están en constante evolución, esta necesidad se vuelve más crítica que nunca.",
    "timeline-2-title": "Fundamos FavForMe",
    "timeline-2-description":
      "Establecimos una fundación orientada a apoyar a aquellos que brindan ayuda, mediante la creación de campañas solidarias de crowdfunding en colaboración con empresas comprometidas con la responsabilidad social.",
    "timeline-2-button": "Ver sitio web",
    "timeline-3-title": "Graduados de Founder Institute Buenos Aires",
    "timeline-3-description":
      "Nos sumamos al programa de aceleración de tres meses, conectando con el vibrante ecosistema emprendedor de Buenos Aires y estableciendo vínculos con posibles socios.",
    "footer-title": "Queremos conocer tu proyecto, podemos ayudarte",
    "footer-meeting": "Coordinemos una reunión",
    "footer-mail": "Envianos un correo",
  },
} as const;
